import React from 'react';
import moment from 'moment';
import uuidv4 from 'uuid/v4';

import './MessageInput.css';


class MessageInput extends React.Component {
  constructor(props) {
    super(props);

    this.onSubmit = this.onSubmit.bind(this);
  }

  onSubmit(event) {
    event.preventDefault();
    let inputMessage = event.target.elements.messageInput;
    const newMessage = {
      user: 'Pavlo',
      id: uuidv4(),
      created_at: moment().format('YYYY-MM-DD HH:mm'),
      message: inputMessage.value
    };
    if(inputMessage.value) {
      this.props.setState(prevState => {
        return {
          messages: [...prevState.messages, newMessage]
        };
      });
    }

    inputMessage.value = '';
  } 

  render() {
    return(
      <div className="type-msg">
        <form className="input-msg-write" onSubmit={this.onSubmit}>
          <textarea type="text" className="write-msg" name="messageInput" placeholder="Type a message" />
          <button className="msg-send-btn" type="submit"><i className="fa fa-paper-plane-o" aria-hidden="true"></i></button>
        </form>
      </div>
    );
  }
}

export default MessageInput;