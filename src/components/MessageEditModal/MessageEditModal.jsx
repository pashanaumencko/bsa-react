import React from 'react';


class MessageEditModal extends React.Component {

  // constructor(props) {
  //   super(props);

  //   // this.onSubmit = this.onSubmit.bind(this);
  // }

  // onSubmit(event) {
  //   event.preventDefault();
  //   let inputMessage = event.target.elements.messageInput;
  //   const newMessage = {
  //     user: 'Pavlo',
  //     id: uuidv4(),
  //     created_at: moment().format('YYYY-MM-DD HH:mm'),
  //     message: inputMessage.value
  //   };
  //   if(inputMessage.value) {
  //     this.setState(prevState => {
  //       return {
  //         messages: [...prevState.messages, newMessage]
  //       };
  //     });
  //   }

  //   inputMessage.value = '';
  // } 

  render() {
    return (
      <div className="modal fade" id="messageEditModal" tabIndex="-1" role="dialog" aria-labelledby="messageEditModal" aria-hidden="true">
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="messageEditModal">Edit message</h5>
              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                  <label htmlFor="message-text" className="col-form-label">Message:</label>
                  <textarea className="form-control" id="message-text" defaultValue={this.props.messageText} ></textarea>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-primary">Send message</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default MessageEditModal;
