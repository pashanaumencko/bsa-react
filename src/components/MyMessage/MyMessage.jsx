import React from 'react';
import MessageEditModal from '../MessageEditModal/MessageEditModal';

import './MyMessage.css';

class MyMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalShow: false
    };

    this.onDeleteClick = this.onDeleteClick.bind(this);
    this.onEditClick = this.onEditClick.bind(this);
  }

  onDeleteClick(event, id) {
    const array = [...this.props.messages].filter(message => !(message.id === id));
    this.props.setState({ messages: array });
  }

  onEditClick(event) {
    this.setState({ isModalShow: true });
  }

  render() {
    const { user:userName, message:messageText, created_at:messageDate } = this.props.message;

    return(
      <div className="outgoing-msg">
        <div className="sent-msg">
          <p>
            <h6>{userName}</h6>
            {messageText}
          </p>
          <div className="msg-info">
            <span className="time-date">{messageDate}</span>
            <div className="msg-actions">
              <div className="msg-delete" onClick={event => this.onDeleteClick(event, this.props.id)}>
                <i className="fa fa-trash"></i>
              </div>
              <div className="msg-edit" onClick={this.onEditClick}>
                <i className="fa fa-edit"></i>
              </div>
            </div>
          </div>
        </div>
        {this.state.isModalShow ? <MessageEditModal messageText={messageText} setState={this.props.setState} /> : null}
      </div>
    );
  }
}

export default MyMessage;