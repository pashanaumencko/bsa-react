import React from 'react';
import Message from './../Message/Message';
import MyMessage from './../MyMessage/MyMessage';

import './MessageList.css';

class MessageList extends React.Component {

  getMessageList() {
    const messages = this.props.messages;

    return messages.reverse().map(message => {
      const { id, user } = message;
      return (user === 'Pavlo') 
        ? <MyMessage 
            key={id} 
            id={id}
            messages={messages}
            message={message}
            setState={this.props.setState} />
        : <Message 
            key={id} 
            id={id}
            message={message} />;
    });
  }

  render() {
    return (
      <div className="message-list">
          {this.getMessageList()}
      </div>
    );
  }
}

export default MessageList;