import React from 'react';
import Header from './components/Header/Header';
import moment from 'moment';
import MessageList from './components/MessageList/MessageList';
import MessageInput from './components/MessageInput/MessageInput';
import Spinner from './components/Spinner/Spinner';

import './Chat.css';

class Сhat extends React.Component {
  constructor () {
    super();
    this.state = {
      messages: [],
      isModalShow: false,
      isLoaded: false
    };

    this.setState = this.setState.bind(this);
  }

  componentDidMount() {
    const API_URL = 'https://api.myjson.com/bins/1hiqin.json';

    fetch(API_URL)
      .then(res => res.json())
      .then(json => {
        this.setState({ messages: json, isLoaded: true});
      })
      .catch(error => console.log(error));

    console.log(this.state.messages);
  }

  getSortedMessagesByDate() {
    const timeFormat = 'YYYY-MM-DD HH:mm';
    const messages = this.state.messages;
    return messages
      .sort((firstMessage, secondMessage) => {
        const firstDate = firstMessage.created_at;
        const secondDate = secondMessage.created_at;
        return moment(secondDate, timeFormat).diff(firstDate, timeFormat);
      });
  }

  getHeader() {
    const messages = this.getSortedMessagesByDate();
    console.log(messages);
    return <Header messages={messages} />;
  }

  getMessageList() {
    const messages = this.getSortedMessagesByDate();
    return <MessageList messages={messages} setState={this.setState} />;
  }

  getMessageInput() {
    return <MessageInput setState={this.setState} onSubmit={this.onSubmit} />
  }

  render() {
    return this.state.isLoaded 
      ? (
        <div className="container">
          <div className="chat">
            {this.getHeader()}
            {this.getMessageList()}
            {this.getMessageInput()}
            {}
          </div>
        </div>
        )
      : <Spinner />; 
  }
}

export default Сhat;
